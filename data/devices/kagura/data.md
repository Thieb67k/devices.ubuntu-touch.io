---
name: "Sony Xperia XZ (F8331 & F8332)"
comment: "community device"
deviceType: "phone"

deviceInfo:
  - id: "cpu"
    value: "2x2.15 GHz Kryo, 2x1.6 GHz Kryo"
  - id: "chipset"
    value: "Qualcomm Snapdragon 820 MSM8996"
  - id: "gpu"
    value: "Qualcomm Adreno 530"
  - id: "rom"
    value: "32/64GB"
  - id: "ram"
    value: "3GB"
  - id: "android"
    value: "7.1 AOSP"
  - id: "battery"
    value: "2900 mAh"
  - id: "display"
    value: "1080x1920 pixels, 5.2 in"
  - id: "rearCamera"
    value: "23MP"
  - id: "frontCamera"
    value: "13MP"
contributors:
  - name: "konradybcio"
    forum: "https://forums.ubports.com/user/konradybcio"
  - name: "sjllls"
sources:
  portType: "external"
externalLinks:
  - name: "Device Subforum"
    link: "https://forums.ubports.com/topic/5004/xperia-xz-kagura-f8331-f8332"
  - name: "Device source - kagura"
    link: "https://github.com/konradybcio/device-sony-kagura"
  - name: "Device source - tone platform"
    link: "https://github.com/konradybcio/device-sony-tone"
  - name: "Kernel source"
    link: "https://github.com/fredldotme/device-sony-common"
---
