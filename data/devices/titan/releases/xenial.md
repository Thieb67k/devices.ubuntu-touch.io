---
installLink: "https://forum.xda-developers.com/t/rom-ubuntu-touch-vivid-10-02-2017-ubuntu-touch-for-titan.3608846/"

portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "?"
      - id: "dataConnection"
        value: "?"
      - id: "dualSim"
        value: "?"
      - id: "calls"
        value: "?"
      - id: "mms"
        value: "?"
      - id: "pinUnlock"
        value: "?"
      - id: "sms"
        value: "?"
      - id: "audioRoutings"
        value: "?"
      - id: "voiceCall"
        value: "?"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "?"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "-"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "-"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "sdCard"
        value: "x"
      - id: "rtcTime"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "-"
      - id: "wirelessExternalMonitor"
        value: "-"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "?"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "?"
      - id: "nfc"
        value: "-"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "-"
      - id: "gps"
        value: "?"
      - id: "proximity"
        value: "?"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "-"
---

### Installation steps

You can install Ubuntu Touch (Xenial) on the version XT1068 Moto G2.
Before, you have to take a number of preparatory steps:

1. You need to use Ubuntu on your PC AND a Motorola Moto G (2014) and an usb cable
2. Enable developer mode on the phone.
3. Connect PC and phone via usb.
4. Install ADB on your computer.
5. Unlock bootloader (see Motorola).Change the data partition of the phone to EXT4. Use TWRP.
6. Change the data partition of the phone to EXT4. Recommandation: Use TWRP.
7. Reboot your device into bootloader mode. (Or use buttons Vol- AND Pwr).
8. Install ubuntu-device-flash on your PC: apt-get install ubuntu-device-flash
9. Flash UT: ubuntu-device-flash --verbose --server=https://system-image.ubports.com touch --device=titan --channel=16.04/community/walid/devel --bootstrap
