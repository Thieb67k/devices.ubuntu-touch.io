---
variantOf: "enchilada"
name: "OnePlus 6T"
description: "The OnePlus 6-series is the flagship series made by OnePlus in 2018 year. Flagship CPU Snapdragon 845 in combination with powerful battery provide good daily Ubuntu Touch usage experience. With 6” AMOLED screen and loud speaker you can watch any video with pleasure. What else? Of course good quality 16MP camera and Type-C USB"

deviceInfo:
  - id: "rom"
    value: "128/256 GB UFS2.1"
  - id: "battery"
    value: "Non-removable Li-Po 3700 mAh"
  - id: "display"
    value: "1080x2340 pixel, 6.41 in"
  - id: "dimensions"
    value: "157.50 x 74.80 x 8.20"
  - id: "weight"
    value: "185 g"
---
