---
name: "Bq Aquaris E4.5"
deviceType: "phone"
description: "A solid entry-level smartphone. The Bq E4.5 was one of the first commercially available Ubuntu Touch devices!"
subforum: "71/bq-e4-5"

deviceInfo:
  - id: "cpu"
    value: "Arm Cortex-A7 Quadcore @1300MHz"
  - id: "chipset"
    value: "MediaTek MT6582"
  - id: "gpu"
    value: "ARM Mali-400 MP2 Dual Core @500MHz"
  - id: "rom"
    value: "8 GB"
  - id: "ram"
    value: "1 GB"
  - id: "android"
    value: "Android 4.0, upgradable to Android 4.4.2"
  - id: "battery"
    value: "Li-Po 2150 mAh, removable"
  - id: "display"
    value: "4.5 inches, 55.8 cm2 (~60,8% screen-to-body ratio), 540 x 960 pixels, 16:9 ratio (~245 ppi density)"
  - id: "rearCamera"
    value: "8MP 3264x2448, 2MP 1920x1080@30fps"
  - id: "frontCamera"
    value: "5MP 2560x1920"
  - id: "arch"
    value: "armV7"
  - id: "dimensions"
    value: "137 x 67 x 9 mm (5.39 x 2.64 x 0.35 in)"
  - id: "weight"
    value: "123 g (4.34 oz)"

contributors:
  - name: "BQ"
    role: "Phone maker"
    expired: true

seo:
  description: "A solid entry-level smartphone. The Bq E4.5 was one of the first commercially available Ubuntu Touch devices"
  keywords: "Ubuntu Touch, Aquaris Bq E4.5, Krillin, Linux on Mobile"
---
