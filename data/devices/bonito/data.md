---
variantOf: "sargo"
name: "Google Pixel 3a XL"
description: "The Pixel 3a XL is available in Black, Clearly White, and Purple-ish. Armed with a Snapdragon 670 processor and 4GB of RAM, it has a good price for a Linux phone. What else? An adequate battery capacity of 3,700 mAh, vivid 6-inch OLED display, Type-C USB, headphone jack, and good camera quality."

deviceInfo:
  - id: "battery"
    value: "3700 mAh"
  - id: "display"
    value: "1080x2160 pixels, 6 in"
  - id: "dimensions"
    value: "160mm x 76.2mm x 7.6mm"
  - id: "weight"
    value: "167 g"
---
