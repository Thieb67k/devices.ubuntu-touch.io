---
name: "Meizu Pro 5"
deviceType: "phone"
description: "Meizu PRO5 was the second Meizu device available with UT. Processing power is sufficient for most current tasks as a daily driver. Wireless external display works well with a MS WIDI adapter."
image: "https://topcom.lt/wp-content/uploads/2016/02/Meizu-Pro-5-Ubuntu-Edition.jpg"

deviceInfo:
  - id: "cpu"
    value: "Octa-core (4x2.1 GHz Cortex-A57 & 4x1.5 GHz Cortex-A53)"
  - id: "chipset"
    value: "Exynos 7420 (14 nm)"
  - id: "gpu"
    value: "Mali T760"
  - id: "rom"
    value: "32/64GB"
  - id: "ram"
    value: "3GB/4GB"
  - id: "android"
    value: "5.1"
  - id: "battery"
    value: "3050 mAh"
  - id: "display"
    value: "IPS LCD, 5.7 inches, 1920 x 1080 pixels, 387 ppi density"
  - id: "arch"
    value: "arm"
  - id: "rearCamera"
    value: "21.16MP"
  - id: "frontCamera"
    value: "5MP"
  - id: "dimensions"
    value: "156.7 x 78 x 7.5 mm (6.17 x 3.07 x 0.30 in)"
  - id: "weight"
    value: "168 g (5.93 oz)"

contributors:
  - name: "Meizu"
    role: "Phone maker"
    expired: true
---
