import pMemoize from "p-memoize";

const ansiCodes = {
  yellowFG: "\x1b[33m",
  redFG: "\x1b[31m",
  greenFG: "\x1b[32m",
  reset: "\x1b[0m"
};

async function fetchData(memoCode) {
  const data = {
    installerData: await downloadInstallerData(),
    releaseChannelData: await downloadReleaseChannelData(),
    pushServerStats: await downloadPushServerStats(),
    currencyConversions: await downloadCurrencyConvertionRates("USD")
  };
  return data;
}

// Download installer data
async function downloadInstallerData() {
  try {
    const configs = await fetch(
      "https://ubports.github.io/installer-configs/v2/"
    );
    const aliases = await fetch(
      "https://ubports.github.io/installer-configs/v2/aliases.json"
    );
    return {
      config: await configs.json(),
      alias: await aliases.json()
    };
  } catch (e) {
    // Display a warning if installer data wasn't downloaded
    if (process.argv.includes("offline")) {
      console.log(
        ansiCodes.yellowFG + "%s" + ansiCodes.reset,
        "Installer data unavailable at build time.\n",
        "- If you need to fetch installer data connect to the internet."
      );
      return [];
    } else {
      console.log(
        ansiCodes.redFG + "%s" + ansiCodes.reset,
        "Failed to download installer data!\n",
        "- If you want to ignore this error add the 'offline' parameter."
      );
      process.exit(2);
    }
  }
}

// Download release channel data
async function downloadReleaseChannelData() {
  try {
    const response = await fetch(
      "https://system-image.ubports.com/channels.json"
    );
    const data = await response.json();
    return data;
  } catch (e) {
    // Display a warning if release channel data wasn't downloaded
    console.log(
      ansiCodes.yellowFG + "%s" + ansiCodes.reset,
      "Failed to download release channel data."
    );
    return {};
  }
}

// Downloads usage statistics from push server
async function downloadPushServerStats() {
  // Check token presence
  if (!process.env.PUSHSERVER_AUTHCODE) {
    console.log(
      ansiCodes.yellowFG + "%s" + ansiCodes.reset,
      "Pushserver authcode unavailable at build time!\n",
      "- The website will assume that you are building in an untrusted environment."
    );
    return null;
  }

  // Make request
  try {
    const response = await fetch("https://push.ubports.com/stats", {
      headers: {
        Authorization: "Basic " + process.env.PUSHSERVER_AUTHCODE
      }
    });
    const data = await response.json();
    return data;
  } catch (e) {
    console.log(
      ansiCodes.redFG + "%s" + ansiCodes.reset,
      "Push server stats download failed!"
    );
    process.exit(2);
  }
}

// Get currency conversion rates
// Thanks to https://www.exchangerate-api.com/ for the open API
// https://open.er-api.com/v6/latest/USD
async function downloadCurrencyConvertionRates(currency) {
  try {
    const response = await fetch(
      "https://open.er-api.com/v6/latest/" + currency
    );
    const data = await response.json();
    if (data.result != "success") throw true;
    return data.rates;
  } catch (e) {
    // Display a warning if currency conversion data wasn't downloaded
    console.log(
      ansiCodes.yellowFG + "%s" + ansiCodes.reset,
      "Failed to download currency conversion data."
    );
    return {};
  }
}

export default pMemoize(fetchData);
