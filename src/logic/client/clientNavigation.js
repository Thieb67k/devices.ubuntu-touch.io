var availableContainers, currentPath;

export function onLoadAndNavigation(f) {
  window.addEventListener("load", f);
  window.addEventListener("spa:navigation", f);
}

export function navigate(link, isPop) {
  if (new URL(link).pathname.replace(/\/+$/, "") == currentPath) return;
  // Import next page
  fetch(link)
    .then((response) => response.text())
    .then((data) => {
      // Replace container and head with the new page's
      // Note: scripts and styles are not replaced, they are fully loaded on first page load
      const doc = new DOMParser().parseFromString(data, "text/html");
      let container = getContainers(
        getContainers(availableContainers, doc),
        document
      )[0];
      document
        .querySelector(container)
        .replaceWith(doc.querySelector(container));
      document.title = doc.title;
      document.querySelectorAll("meta").forEach((m) => m.remove());
      doc.querySelectorAll("meta").forEach((m) => document.head.appendChild(m));

      if (!isPop) history.pushState({}, "", link);
      currentPath = new URL(link).pathname.replace(/\/+$/, "");
      window.dispatchEvent(new Event("spa:navigation"));
    });
}

export function initNavigation(preferredContainers) {
  // Set custom client navigation
  availableContainers = preferredContainers;
  currentPath = window.location.pathname.replace(/\/+$/, "");

  // Get preferred container and push content without changing state
  addEventListener("popstate", (event) => {
    navigate(window.location, true);
  });

  onLoadAndNavigation(() => {
    [...document.links]
      .filter((link) => isInternalLink(link))
      .forEach((link) => {
        link.onclick = (e) => {
          e.preventDefault();
          navigate(link.href);
        };
      });
  });
}

export function componentScriptLoader(container, componentScript) {
  console.log("registering " + container);

  onLoadAndNavigation(() => {
    let component = document.querySelector(container);
    if (!!component && !component.classList.contains("script-registered")) {
      console.log("running " + container);
      componentScript();
      component.classList.add("script-registered");
    }
  });
}

export function componentScriptLoadAll(container, componentScript) {
  console.log("registering " + container);

  onLoadAndNavigation(() => {
    document
      .querySelectorAll(container + ":not(.script-registered)")
      .forEach((c) => {
        if (!!c) {
          console.log("running " + container);
          componentScript(c);
          c.classList.add("script-registered");
        }
      });
  });
}

function isInternalLink(link) {
  return (
    link.host == window.location.host &&
    !(link.pathname == window.location.pathname && link.hash) &&
    link.dataset.ignoreNavigation !== ""
  );
}

function getContainers(containers, doc) {
  return containers.filter((container) => !!doc.querySelector(container));
}
